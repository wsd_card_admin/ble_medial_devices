package com.awesomeproject.bluetooth;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.inuker.bluetooth.library.BluetoothClient;
import com.inuker.bluetooth.library.beacon.Beacon;
import com.inuker.bluetooth.library.connect.listener.BleConnectStatusListener;
import com.inuker.bluetooth.library.connect.options.BleConnectOptions;
import com.inuker.bluetooth.library.connect.response.BleConnectResponse;
import com.inuker.bluetooth.library.connect.response.BleNotifyResponse;
import com.inuker.bluetooth.library.model.BleGattProfile;
import com.inuker.bluetooth.library.receiver.listener.BluetoothBondListener;
import com.inuker.bluetooth.library.search.SearchRequest;
import com.inuker.bluetooth.library.search.SearchResult;
import com.inuker.bluetooth.library.search.response.SearchResponse;
import com.medxing.sdk.resolve.TypeCastUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static com.inuker.bluetooth.library.Constants.REQUEST_SUCCESS;
import static com.inuker.bluetooth.library.Constants.STATUS_CONNECTED;
import static com.inuker.bluetooth.library.Constants.STATUS_DEVICE_CONNECTING;
import static com.inuker.bluetooth.library.Constants.STATUS_DEVICE_DISCONNECTING;
import static com.inuker.bluetooth.library.Constants.STATUS_DISCONNECTED;
import static com.inuker.bluetooth.library.Constants.STATUS_UNKNOWN;

/**
 * Created by CJW on 2018/4/24.
 */

public class BluetoothService extends Service {

    private static final String TAG = "cjw_BluetoothService";
    //搜索到设备
    public static final String BLUETOOTH_DEVICES_FOUNDED = "com.medxing.nsble.bluetooth.BLUETOOTH_DEVICES_FOUNDED";
    //设备连接成功
    public static final String BLUETOOTH_DEVICES_CONNECTED = "com.medxing.nsble.bluetooth.BLUETOOTH_DEVICES_CONNECTED";
    public static final String BLUETOOTH_DEVICES_CONNECTING = "com.medxing.nsble.bluetooth.BLUETOOTH_DEVICES_CONNECTING";
    public static final String BLUETOOTH_DEVICES_CONNECTION_FAILED = "com.medxing.nsble.bluetooth.BLUETOOTH_DEVICES_CONNECTION_FAILED";
    public static final String BLUETOOTH_DEVICES_CONNECTION_TIMEOUT = "com.medxing.nsble.bluetooth.BLUETOOTH_DEVICES_CONNECTION_TIMEOUT";
    public static final String BLUETOOTH_DEVICES_DISCONNECTED = "com.medxing.nsble.bluetooth.BLUETOOTH_DEVICES_DISCONNECTED";
    public static final String BLUETOOTH_DEVICES_DISCONNECTING = "com.medxing.nsble.bluetooth.BLUETOOTH_DEVICES_DISCONNECTING";
    public static final String BLUETOOTH_DEVICES_NOT_CONNECTED = "com.medxing.nsble.bluetooth.BLUETOOTH_DEVICES_NOT_CONNECTED";
    public static final String BLUETOOTH_DEVICES_NOTIFY_DATA = "com.medxing.nsble.bluetooth.BLUETOOTH_DEVICES_NOTIFY_DATA";

    private final IBinder binder = new LocalBinder(); // 服务绑定器
    private Set<SearchResult> deviceList = null;//搜索到的蓝牙设备
    private Set<Beacon> beaconList = null;//由SearchResult转换的数据，能获取蓝牙名称等基本信息
    public static BluetoothClient mClient = null;
    private BluetoothChipType bluetoothChipType = new BluetoothChipType();
    private onFindListener onFindListener = null;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initBluetooth();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        closeNotify();
        closeBluetooth();
    }

    /**
     * 配置服务连接监听器类
     */
    public class LocalBinder extends Binder {
        public BluetoothService getService() {
            return BluetoothService.this;

        }
    }

    private void initBluetooth(){
        if (mClient == null)
        mClient = new BluetoothClient(this);
        mClient.registerBluetoothBondListener(bluetoothBondListener);

    }

    private void closeBluetooth(){
        mClient.unregisterBluetoothBondListener(bluetoothBondListener);
    }

    /**
     * 打开蓝牙
     */
    public void openBluetooth(){
        mClient.openBluetooth();
    }



    /**
     * 搜索蓝牙设备
     */
    public void findBluetooth(BluetoothService.onFindListener onFindListener){
        if (mClient.isBluetoothOpened()){
            mClient.openBluetooth();
        }
        SearchRequest request = new SearchRequest.Builder()
                .searchBluetoothLeDevice(3000, 1000)   // 先扫BLE设备3次，每次3s
//                .searchBluetoothClassicDevice(5000) // 再扫经典蓝牙5s
//                .searchBluetoothLeDevice(2000)      // 再扫BLE设备2s
                .build();

        mClient.search(request, new SearchResponse() {
            @Override
            public void onSearchStarted() {
                deviceList = new HashSet<>();
                beaconList = new HashSet<>();
                Log.i(TAG, "onSearchStarted: " + "蓝牙搜索开始");
            }

            //刷新逻辑，每扫描到30次设备，刷新一次数据
            @Override
            public void onDeviceFounded(SearchResult device) {
                if (device.getName().toUpperCase().contains("MEDXING")){
                    deviceList.add(device);
                    Log.i(TAG, "onDeviceFounded: " + "蓝牙到设备" + device.getName());
                    onFindListener.onFounded(getDeviceList());
                }
            }

            @Override
            public void onSearchStopped() {
                onFindListener.onFounded(getDeviceList());
                Log.i(TAG, "onSearchStopped: " + "蓝牙搜索结束");
            }

            @Override
            public void onSearchCanceled() {
                Log.i(TAG, "onSearchStopped: " + "蓝牙搜索取消");
            }
        });
    }

    /**
     * 停止蓝牙搜索
     */
    public void findBluetoothStop(){
        if (mClient != null) {
            mClient.stopSearch();
        }
    }

    /**
     * 获取搜索到的蓝牙设备
     */
    public List<SearchResult> getDeviceList(){

        List<SearchResult> list = new ArrayList<>();
        list.addAll(deviceList);
        return list;
    }

    /**
     * 设备连接监听器
     */
    private final BluetoothBondListener bluetoothBondListener = new BluetoothBondListener() {
        @Override
        public void onBondStateChanged(String mac, int bondState) {
            Log.i(TAG, "onBondStateChanged: " + mac + " bondState:" + bondState);
            bluetoothChipType.setMAC(mac);
        }
    };

    /**
     * 设备配对
     */
    public void connectBleForMac(String mac, BleConnectResponse bleConnectResponse){
        BleConnectOptions options = new BleConnectOptions.Builder()
                .setConnectRetry(3)
                .setConnectTimeout(30000)
                .setServiceDiscoverRetry(3)
                .setServiceDiscoverTimeout(20000)
                .build();
        bluetoothChipType.setMAC(mac);
        mClient.connect(mac,options,bleConnectResponse);
        mClient.registerConnectStatusListener(mac, mBleConnectStatusListener);
    }

    /**
     * 断开连接
     */
    public void disConnect(String mac){
        mClient.disconnect(mac);
    }

    /**
     * 蓝牙配对状态监听
     * 连接或者断开连接
     */
    private final BleConnectStatusListener mBleConnectStatusListener = new BleConnectStatusListener() {
        @Override
        public void onConnectStatusChanged(String mac, int status) {
            if (status == STATUS_CONNECTED){

            }else if (status == STATUS_DEVICE_CONNECTING){

            }else if (status == STATUS_DEVICE_DISCONNECTING){
                Intent intent1 = new Intent(BLUETOOTH_DEVICES_DISCONNECTED);
                sendBroadcast(intent1);
            }else if (status == STATUS_DISCONNECTED){
                bluetoothChipType = new BluetoothChipType();
                Intent intent1 = new Intent(BLUETOOTH_DEVICES_DISCONNECTED);
                sendBroadcast(intent1);
            }else if (status == STATUS_UNKNOWN){

            }
        }
    };

    /**
     * 主动获取设备连接状态
     * //Constants.STATUS_UNKNOWN
     * //Constants.STATUS_DEVICE_CONNECTED
     * //Constants.STATUS_DEVICE_CONNECTING
     * //Constants.STATUS_DEVICE_DISCONNECTING
     * //Constants.STATUS_DEVICE_DISCONNECTED
     */
    private int getDeviceConnectStatus(String Mac){
        return mClient.getConnectStatus(Mac);
    }

    /**
     * 蓝牙相关知识
     * BLE分为三部分：Service，Characteristic，Descriptor。这三部分都用UUID作为唯一标识符。
     * UUID为这种格式：0000ffe1-0000-1000-8000-00805f9b34fb。比如有3个Service，那么就有三个不同的UUID与Service对应。
     * 这些UUID都写在硬件里，我们通过BLE提供的API可以读取到。
     *      一个BLE终端可以包含多个Service， 一个Service可以包含多个Characteristic，
     *      一个Characteristic包含一个value和多个Descriptor，一个Descriptor包含一个Value。
     *      Characteristic是比较重要的，是手机与BLE终端交换数据的关键，读取设置数据等操作都是操作Characteristic的相关属性。
     */
    /**
     * 主动读Characteristic
     * 读取数据
     * 这里我们可以不用主动读Characteristic，我们使用通知监听，
     */
//    public class ReadCharacteristicThread extends Thread{
//
//        @Override
//        public void run() {
//            while (true) {
//                if (bluetoothChipType.isAvailable())
//                mClient.read(bluetoothChipType.getMAC(),bluetoothChipType.getServiceUUID(),bluetoothChipType.getClientUUID(),(int code,byte[] data) -> {
//                    if (code == REQUEST_SUCCESS){
//                        Log.i(TAG, "run: " + TypeCastUtil.bytesToHexString(data));
//                    }
//                });
//            }
//        }
//
//    }


    /**
     * 写Characteristic
     * 写入数据
     */
    public void writeCharacter(byte[] cmd){
        if (bluetoothChipType.isAvailable())
            mClient.write(bluetoothChipType.getMAC(), bluetoothChipType.getServiceUUID(), bluetoothChipType.getWriteUUID(), cmd, code -> {
                if (code == REQUEST_SUCCESS){
                    Log.i(TAG, "run: " + "数据写入成功" + TypeCastUtil.bytesToHexString(cmd));
                }
            });
        else {
            sendBroadcast(new Intent(BLUETOOTH_DEVICES_NOT_CONNECTED));
        }
    }

    /**
     * 打开Notify
     */
    public void startNotify(){
        if (bluetoothChipType.isAvailable()){
            mClient.notify(bluetoothChipType.getMAC(), bluetoothChipType.getServiceUUID(), bluetoothChipType.getNotificationUUID(), new BleNotifyResponse() {
                @Override
                public void onNotify(UUID service, UUID character, byte[] value) {
                    Log.i(TAG, "run: " + TypeCastUtil.bytesToHexString(value));
                    Intent intent = new Intent(BLUETOOTH_DEVICES_NOTIFY_DATA);
                    intent.putExtra(BLUETOOTH_DEVICES_NOTIFY_DATA,value);
                    sendBroadcast(intent);
                }

                @Override
                public void onResponse(int code) {
                }
            });
        }else {
            sendBroadcast(new Intent(BLUETOOTH_DEVICES_NOT_CONNECTED));
        }
    }

    /**
     * 关掉Notify
     */
    public void closeNotify(){
        if (bluetoothChipType.isAvailable()){
            mClient.unnotify(bluetoothChipType.getMAC(), bluetoothChipType.getServiceUUID(), bluetoothChipType.getNotificationUUID(), (code) ->{
                if (code == REQUEST_SUCCESS){

                }
            });
        }else {
            sendBroadcast(new Intent(BLUETOOTH_DEVICES_NOT_CONNECTED));
        }
    }

    /**
     * 获取信号强度
     */
    public void getRssi(){
        if (bluetoothChipType.isAvailable())
            mClient.readRssi(bluetoothChipType.getMAC(), (code, data) -> {

            });
        else {
            sendBroadcast(new Intent(BLUETOOTH_DEVICES_NOT_CONNECTED));
        }
    }

    /**
     * 搜索设备的接口
     */
    public interface onFindListener{
        void onFounded(List<SearchResult> bleDevices);
    }

    /**
     * 确定芯片类型
     * 获取读写的UUID
     */
    public int checkChipType(BleGattProfile bleGattProfile){
        int chipType = BluetoothUtils.checkChipType(bleGattProfile);
        if (chipType > -1){
            BluetoothUtils.setBluetoothChipType(bluetoothChipType);
            startNotify();
        }else {
            return -1;
        }
        return chipType;
    }

}
