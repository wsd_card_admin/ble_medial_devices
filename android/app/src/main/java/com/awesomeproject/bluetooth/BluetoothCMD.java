package com.awesomeproject.bluetooth;

/**
 * Created by user on 2018/6/19.
 * MEDXING设备写入蓝牙协议
 * 变量名中含DEVICE的是设备往手机发的
 * 变量名中含BLE的是手机往设备发的命令
 */

public class BluetoothCMD {

    /**
     * 设备ID
     */
    public static final byte SPO2_ID = 0x23;
    public static final byte NIBP_ID = 0x20;
    public static final byte IRT_ID = 0x22;
    public static final byte BMI_ID = 0x2A;
    public static final byte ECG_ID = 0x24;
    public static final byte BGM_ID = 0x25;

    /**
     *  体脂秤脂称协议
     */
    //手机进入设备就绪状态，先发送100个00H后发送命令，间隔为300ms
    public static final byte[] BMI_DEVICE_READY_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFE,0x04, (byte) 0x8F,0x2A,0x61};
    public static final byte BMI_DEVICE_READY = 0x61;
    //手机发送开始命令，设备停止发送就绪命令
    public static final byte[] BMI_BLE_START_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFE,0x04, (byte) 0xB5,0x01, (byte) 0xB0};
    //确认开始测量
    public static final byte BMI_BLE_START_OK = 0x60;
    //手机发送检查连接
    public static final byte[] BMI_BLE_CHECK_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFE,0x04, (byte) 0xB9,0x01, (byte) 0xB4};
    //收到数据
    //data（4）的结果依次为：重量H、重量L、阻抗H、阻抗L说明：重量H、重量L:H是整数，L是小数，单位是Kg阻抗H、阻抗L:0x0101代表256欧
    public static final byte BMI_DEVICE_RESULT = 0x65;
    //测量错误错误值
    //DATA：错误码未设置，只要接收到此命令只显示错误即可
    public static final byte BMI_DEVICE_ERROR = 0x66;


    /**
     * 温度计
     */
    //手机进入设备就绪状态，先发送100个00H后发送命令，间隔为300ms
    public static final byte[] IRT_DEVICE_READY_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFE,0x04, (byte) 0x87,0x22,0x61};
    public static final byte IRT_DEVICE_READY = 0x61;
    //手机发送开始命令，设备停止发送就绪命令
    public static final byte[] IRT_BLE_START_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFE,0x04, (byte) 0xB5,0x01, (byte) 0xB0};
    //设备确认开始测量
    public static final byte IRT_DEVICE_START_OK = 0x60;
    //确认开始测试，并进入测试状态 -->鬼知道这条命令有什么用
    public static final byte[] IRT_BLE_MEASUREING_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFE,0x04, (byte) 0xB6,0x01, (byte) 0xB1};
    //DATA：错误码未设置，只要接收到此命令只显示错误即可

    public static final byte IRT_DEVICE_ERROR = 0x66;
    //0xFF 0xFE 0x05 CS 0x22 0x66 DATA（1byte）
    //返回的温度 DATA：测量温度（2byte）
    public static final byte IRT_DEVICE_RESULT = 0x65;
    //0xFF 0xFE 0x06 CS 0x22 0x65 DATA（2byte）
    //按键测量  测温计按键测量，接收此命令后发送手机按键命令
    public static final byte IRT_DEVICE_CLICK = 0x62;
    //0xFF 0xFE 0x04 0x88 0x22 0x62
    //手机发送检查连接
    public static final byte[] IRT_BLE_CHECK_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFE,0x04, (byte) 0xBC,0x01, (byte) 0xB7};

    /**
     * 血压计
     */
    //手机进入设备就绪状态
    public static final byte[] NIBP_DEVICE_READY_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFA,0x04,0x76,0x20,0x51};
    public static final byte NIBP_DEVICE_READY = 0x51;
    //设备请求开始测量
    public static final byte[] NIBP_DEVICE_START_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFA,0x04,0x77,0x20,0x52};
    public static final byte NIBP_DEVICE_START = 0x52;
    //手机请求开始测量
    public static final byte[] NIBP_BLE_START_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFA,0x04, (byte) 0xA5,0x01, (byte) 0xA0};
    //设备确定开始测量
    public static final byte[] NIBP_DEVICE_START_OK_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFA,0x04, (byte) 0x75,0x20, (byte) 0x50};
    public static final byte NIBP_DEVICE_START_OK = 0x50;
    //实时压力
    public static final byte NIBP_DEVICE_REAL_PRESSURE = 0x54;
    //0xFF 0xFA 0x08 CS 0x21 0x54 DATA（4byte）
    //DATA：气压高位，气压地位，心率，心率不齐标志（0x00：心率不齐，0x11无心率不齐）
    //测量结果
    public static final byte NIBP_DEVICE_RESULT_PRESSURE = 0x55;
    //0xFF 0xFA 0x0a CS 0x21 0x54 DATA（6byte）
    //DATA：高压高位，高压低位，低压高位，低压低位，心率，心率不齐标志
    //错误
    public static final byte NIBP_DEVICE_RESULT_ERROR = 0x56;
    //0xFF 0xFA 0x05 CS 0x21 0x56 DATA（1byte）
    //DATA：见错误代码表
    //手机请求停止测量
    public static final byte[] NIBP_BLE_STOP_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFA,0x04, (byte) 0xA8,0x01, (byte) 0xA3};
    //设备确定停止测量
    public static final byte[] NIBP_DEVICE_STOP_OK_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFA,0x04, (byte) 0x78,0x20, (byte) 0x53};
    public static final byte NIBP_DEVICE_STOP_OK = 0x53;
    //手机请求检查连接
    public static final byte[] NIBP_BLE_CHECK_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFA,0x04, (byte) 0xAC,0x01, (byte) 0xA7};
    //设备确定检查连接
    public static final byte[] NIBP_DEVICE_CHECK_OK_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFA,0x04, (byte) 0x76,0x20, (byte) 0x51};

    /**
     * 心电
     */
    //手机进入设备就绪状态
    public static final byte[] ECG_DEVICE_READY_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFE,0x04, (byte) 0xA9,0x24, (byte) 0x81};
    public static final byte ECG_DEVICE_READY = (byte) 0x81;
    //手机进入设备就绪心跳
    public static final byte[] ECG_DEVICE_READY_ACK_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFE,0x04, (byte) 0xAA,0x24, (byte) 0x82};
    //手机请求开始测试
    public static final byte[] ECG_BLE_START_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFE,0x04, (byte) 0xD5,0x01, (byte) 0xD0};
    public static final byte ECG_DEVICE_START_OK = (byte) 0x80;
    //设备确认开始测试
    public static final byte[] ECG_DEVICE_START_OK_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFE,0x04, (byte) 0xA8,0x24, (byte) 0x80};
    //波形数据
    //0xFF 0xFE 0x18 CS 0x24 0x84 DATA（长度不定）
    // DATA：心电波形数据，采样频率150Hz，采样精度8bit：0x80（128）为0mV，每0x28（40）代表1mV，即0xA8（168）为+1mV，0x58（88）为-1mV，其余以此类推。
    public static final byte ECG_DEVICE_WAVE = (byte)0x84;
    //错误
    //0xFF 0xFE 0x07 CS 0x24 0x85 DATA（3byte）
    //DATA：错误（1byte），心率值（2byte）。错误值详见错误代码表；心率值为short，大于300无效。
    public static final byte ECG_DEVICE_RESULT_AND_ERROR = (byte)0x85;

    //手机请求停止测量
    public static final byte[] ECG_BLE_STOP_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFE,0x04, (byte) 0xDC,0x01, (byte) 0xD7};
    //设备确认停止测量
    public static final byte[] ECG_DEVICE_STOP_OK_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFE,0x04, (byte) 0xAB,0x01, (byte) 0x83};
    public static final byte ECG_DEVICE_STOP_OK = (byte) 0x83;
    //手机请求检查连接
    public static final byte[] ECG_BLE_CHECK_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFE,0x04, (byte) 0xDC,0x01, (byte) 0xD7};
    //设备确定检查连接
    public static final byte[] ECG_DEVICE_CHECK_OK_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFE,0x04, (byte) 0xA6,0x21, (byte) 0x81};

    /**
     * 指夹血氧
     * 血氧设备不用主动发送数据，只需要接收就行了
     */
    //血氧的0x95包ID，含基本的心率，连接状态等
    public static final byte SPO2_DEVICE_DATA_INFO = (byte)0x95;
    //血氧的0x96包ID，血氧的波形图
    public static final byte SPO2_DEVICE_DATA_WAVE = (byte)0x96;
    //血氧的0x97包ID，血氧的HRV
    public static final byte SPO2_DEVICE_DATA_HRV = (byte)0x97;

    /**
     * 血糖仪
     */
    //手机进入设备就绪状态
    public static final byte BGM_DEVICE_READY = 0x71;
    //读取试纸状态
    public static final byte[] BGM_BLE_READ_PAPER_ARRAY = new byte[]{(byte) 0xFF, (byte) 0xFE,0x04, (byte) 0xC7,0x01, (byte) 0xC2};
    //返回试纸代码
    public static final byte BGM_DEVICE_READING_PAPER = 0x73;
    //检测试纸状态中
    public static final byte BGM_DEVICE_PAPER_CLICK = 0x78;
    //试纸错误
    public static final byte BGM_TYPE_PAPER_ERROR = 0x74;
    //试纸有效，请采血
    public static final byte BGM_TYPE_PAPER_OK = 0x72;
    //测量开始倒计时
    public static final byte BGM_TYPE_COUNT_DOWN = 0x70;
    //测量错误
    public static final byte BGM_DEVICE_ERROR = 0x76;
    //测量结果DATA2byte [17（data[0]）*256+35（data[1]）]/10=438.7(mmol/L)
    public static final byte BGM_DEVICE_RESULT = 0x75;


}
