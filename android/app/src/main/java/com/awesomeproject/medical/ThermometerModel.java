package com.awesomeproject.medical;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.awesomeproject.bluetooth.BluetoothService;
import com.facebook.react.ReactActivity;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.inuker.bluetooth.library.connect.response.BleConnectResponse;
import com.inuker.bluetooth.library.model.BleGattProfile;
import com.inuker.bluetooth.library.search.SearchResult;
import com.medxing.sdk.resolve.IrtResolve;
import com.medxing.sdk.resolve.ResolveManager;

import java.util.List;

import static android.content.Context.BIND_AUTO_CREATE;
import static com.inuker.bluetooth.library.Constants.REQUEST_FAILED;
import static com.inuker.bluetooth.library.Constants.REQUEST_SUCCESS;
import static com.inuker.bluetooth.library.Constants.REQUEST_TIMEDOUT;

public class ThermometerModel extends ReactContextBaseJavaModule {
  private static ReactApplicationContext reactContext;

  private static final String DURATION_SHORT_KEY = "SHORT";
  private static final String DURATION_LONG_KEY = "LONG";

  public static ResolveManager resolveManager;

  public static BluetoothService bluetoothService = null;

  private List<SearchResult> deviceList = null;//搜索到的蓝牙设备
  Callback cbErrorRefresh;
  Callback cbOKRefresh;



  public ThermometerModel(@NonNull ReactApplicationContext reactContext) {
    super(reactContext);

    if(bluetoothService == null){
      initService();
    }
  }

  /**
   * 设置自动连接的mac地址列表
   */
  public void setAutoConnectList(List<String>  macList){

  }

  /**
   * 刷新蓝牙设备列表
   * @param errorCallback
   * @param successCallback
   */
  @ReactMethod
  public void refreshDevicesList(Callback errorCallback,
                                 Callback successCallback){
    cbErrorRefresh = errorCallback;
    cbOKRefresh = successCallback;

    if (bluetoothService != null){
      bluetoothService.findBluetooth(bleDevices -> {
        deviceList = bleDevices;
        notifyRefreshResult();
      });
    }else {
      initService();
    }
  }

  /**
   * 连接
   * @param mac
   * errorCallback 连接错误，参数： code:int msg:string
   * successCallback 成功,参数：无
   */

  @ReactMethod
  public void connectDevice(String mac,Callback errorCallback,
                             Callback successCallback){
    if(bluetoothService == null){
      if(errorCallback != null){
        errorCallback.invoke(0,"bluetoothService is not valide");
      }
      return;
    }

    bluetoothService.connectBleForMac(mac, new BleConnectResponse() {
      @Override
      public void onResponse(int code, BleGattProfile data) {
        switch (code){
          case REQUEST_SUCCESS:
            if(bluetoothService.checkChipType(data) > -1){
              successCallback.invoke();
            }else {
              errorCallback.invoke(1,"Connect failed:" + mac);
            }
            break;
          case REQUEST_FAILED:
            errorCallback.invoke(2,"Connect failed - REQUEST_FAILED:" + mac);
            break;
          case REQUEST_TIMEDOUT:
            errorCallback.invoke(3,"Connect failed -TimeOut:" + mac);
            break;
        }
      }
    });
  }

  /**
   * 关闭连接
   * @param mac
   */
  public  void closeDevice(String mac){
    if(bluetoothService == null){
      return;
    }
    bluetoothService.disConnect(mac);
  }

  /**
   * 返回已经连接的设备列表
   * @return MAC地址列表
   */
  public void getConnectedDevicesList(Callback errorCallback,
                                              Callback successCallback){

    bluetoothService.getDeviceList();
//    try {
//      successCallback.invoke(relativeX, relativeY, width, height);
//    } catch (IllegalViewOperationException e) {
//      errorCallback.invoke(e.getMessage());
//    }
    return ;
  }



  /**
   * 设置测量结果回调
   */
  @ReactMethod
  public void getMeasureResult(String mac, Callback errorCallback,
                                Callback successCallback){

  }

  // 将蓝牙设备列表回传给js
  private void notifyRefreshResult(){
    if (cbOKRefresh != null){
      if(deviceList != null && deviceList.size() > 0){
        cbOKRefresh.invoke(deviceList.get(0).device.getAddress());
      }else{
        cbOKRefresh.invoke();
      }
    }
    // 随时清理
    cbOKRefresh = null;
    cbErrorRefresh = null;
  }

  private void initService(){
    Intent bluetoothIntent;
    if (bluetoothService == null) {
      bluetoothIntent = new Intent(reactContext, BluetoothService.class);
      reactContext.bindService(bluetoothIntent, bluetoothServiceConnection, BIND_AUTO_CREATE);
    }
  }

  private void initResolve(){

    if(resolveManager == null){
      resolveManager = ResolveManager.getInstance(reactContext.getApplicationContext());
    }
  }

  private void closeService(){
    if (bluetoothService != null) {
      reactContext.unbindService(bluetoothServiceConnection);
      // mBluetoothLEService.Disconnect();
      bluetoothService = null;
    }
  }
  /**
   * 打开蓝牙服务
   */
  private ServiceConnection bluetoothServiceConnection = new ServiceConnection() {
    @Override
    public void onServiceConnected(ComponentName arg0, IBinder service) {
      bluetoothService = ((BluetoothService.LocalBinder) service).getService();
      bluetoothService.findBluetooth(bleDevices -> {
        deviceList = bleDevices;
        notifyRefreshResult();
        initResolve();
      });
    }

    @Override
    public void onServiceDisconnected(ComponentName arg0) {
      if(resolveManager != null){
        resolveManager.close();
        resolveManager = null;
      }
      bluetoothService = null;

    }
  };

  @NonNull
  @Override
  public String getName() {
    return "ThermometerModel";
  }
}